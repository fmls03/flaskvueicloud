from flask import Flask, request, jsonify
import os
from pyicloud import PyiCloudService
from geopy.geocoders import Nominatim

app = Flask(__name__)

sk = str(os.urandom(256))
app.config['SECRET_KEY'] = sk

api = ''

@app.route('/login', methods = ['GET', 'POST'])
def loginIcloud():
    if request.method == 'POST':
        global api
        payload = request.get_json()
        email = payload.get('email')
        password = payload.get('password')
        try:
            api = PyiCloudService(email, password) 
            return jsonify('Logged')
        except Exception as e:
            response = {'string': 'Something goes wrong', 'error': str(e)}
            return jsonify(response)

@app.route('/getData', methods=['GET'])
def getData():
    if request.method == 'GET':
        response = (api.iphone.location())
        geolocator = Nominatim(user_agent='geoapiExercises')
        lat= response['latitude']
        lng= response['longitude']
        coordinates = str(lat) + ", " + str(lng) 
        iphoneLocation = geolocator.reverse(coordinates)
        response.update(api.iphone.status())
        iphoneLocation = {'iphoneLocation': str(iphoneLocation)}
        response.update(iphoneLocation)
        secret_key = {'secret_key': sk}
        response.update(secret_key)
        return jsonify(response)

@app.route('/playSound', methods=['GET'])
def playSound():
    if request.method == 'GET':
        try:
            api.iphone.play_sound()
            return jsonify('iPhone is ringing')
        except Exception as e:
            response = {'string': 'Something goes wrong', 'error': str(e)}
            return jsonify(response)

@app.route('/lockIphoneRemotly', methods=['GET', 'POST'])
def lockIphoneRemotly():
    if request.method == 'POST':
        payload = request.get_json()
        if payload.get('secret_key') == sk:
            try:
                api.iphone.lost_device('3476358827', 'iPhone locked remotly')
                return jsonify('iPhone locked successfully')
            except Exception as e:
                response = {'string': 'Something goes wrong', 'error': str(e)}
                return jsonify(response)
        else:
            return jsonify("You can't lock iPhone")





if __name__ == '__main__':
    app.run(debug=True)